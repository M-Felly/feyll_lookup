export enum CssHelper {
    ACTIVE_CLASS = "is-active",
    HIDE_CLASS = "is-hidden",
}