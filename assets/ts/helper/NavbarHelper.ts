
// class names
export enum NavCssClasses {
 NAV_ITEMS_CLASS_NAME = "navbar-item",
 NAV_DROPDOWN_ICON = "navbar-burger",
 NAV_MENU_CONTAINER = "navbar-menu",
}

// navbar background container
export const NAV_BACKGROUND_CONTAINER = "navbar-background";